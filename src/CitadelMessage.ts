import {PluginId, StreamId} from "./MessageDispatcher";

export type MessageType = "REQUEST" | "RESPONSE";

export class CitadelMessage<T> {
    readonly targetId: PluginId;
    readonly type: MessageType;
    readonly payload: MessagePayload<T>;

    constructor(targetId: PluginId, type: MessageType, payload: MessagePayload<T>) {
        this.targetId = targetId;
        this.type = type;
        this.payload = payload;
    }
}

class MessagePayload<T> {
    readonly streamId: StreamId;
    readonly value: T;

    constructor(streamId: StreamId, value: T) {
        this.streamId = streamId;
        this.value = value;
    }
}

export function packMessage<T>(data: T, type: MessageType, targetId: PluginId, streamId: StreamId): CitadelMessage<T> {
    return new CitadelMessage(
        targetId,
        type,
        new MessagePayload<T>(
            streamId,
            data
        )
    )
}

export class SimpleResponse {
    readonly status: string;

    constructor(status: string) {
        this.status = status;
    }
}

export class EmptyRequest {
}
