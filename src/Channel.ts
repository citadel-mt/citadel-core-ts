import {ChannelDependentDevice} from "./Devices";
import CitadelEvent from "./utils/CitadelEvent";

export interface CitadelChannel {

    sendMessageAsync(message: string): Promise<void>

    readonly onReceiveMessageEvent: CitadelEvent<string>
}

export interface ClientChannel extends CitadelChannel {

    scanDevicesAsync(): Promise<Array<ChannelDependentDevice>>

    connectAsync(device: ChannelDependentDevice): Promise<void>

    disconnectAsync(): Promise<void>
}

export interface ServerChannel extends CitadelChannel {

    initialize()

    readonly onDeviceConnectedEvent: CitadelEvent<ChannelDependentDevice>

    terminate()
}
