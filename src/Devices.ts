export class CitadelClient {
    name?: string;
    type: string;

    constructor(type: string, name: string = undefined) {
        this.name = name;
        this.type = type;
    }
}

export class CitadelServer {
    name?: string;
    type: string;

    constructor(type: string, name: string = undefined) {
        this.name = name;
        this.type = type;
    }
}

export class ChannelDependentDevice {
    id: number;
    name: string;

    constructor(id: number, name: string) {
        this.id = id;
        this.name = name;
    }
}