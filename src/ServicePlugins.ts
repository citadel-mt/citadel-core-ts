import {EmptyRequest, SimpleResponse} from "./CitadelMessage";
import {CitadelClient, CitadelServer} from "./Devices";
import {CitadelPlugin, PluginInfo, Receiver, Sender} from "./CitadelPlugin";
import ResponsableEvent from "./utils/ResponsableEvent";

const pluginId = "com.appspot.magtech.citadel.Service";

const clientConnectionStream = "CLIENT_CONNECTION";
const clientDisconnectionStream = "CLIENT_DISCONNECTION";
const serverTerminationStream = "SERVER_TERMINATION";

@PluginInfo(pluginId)
export class ServiceServerPlugin implements CitadelPlugin {

    @Sender(serverTerminationStream)
    readonly sendTerminateAsync: (message: EmptyRequest) => Promise<EmptyRequest>;

    @Receiver(clientConnectionStream)
    readonly clientConnectedEvent: () => ResponsableEvent<CitadelClient, CitadelServer>;

    @Receiver(clientDisconnectionStream)
    readonly clientDisconnectedEvent: () => ResponsableEvent<CitadelClient, SimpleResponse>;
}

@PluginInfo(pluginId)
export class ServiceClientPlugin implements CitadelPlugin {

    @Sender(clientConnectionStream)
    readonly sendHelloAsync: (data: CitadelClient) => Promise<CitadelServer>;

    @Sender(clientDisconnectionStream)
    readonly sendDisconnectAsync: (data: CitadelClient) => Promise<SimpleResponse>;

    @Receiver(serverTerminationStream)
    readonly serverTerminatedEvent: ResponsableEvent<EmptyRequest, SimpleResponse>
}