import {CitadelChannel} from "./Channel";
import MessageDispatcher, {PluginId, StreamId} from "./MessageDispatcher";
import {packMessage} from "./CitadelMessage";

export interface CitadelPlugin {
}

const defaultStreamId = "NOT_SPECIFIED";

export function PluginInfo(pluginId: PluginId) {
    return function <T extends { new(...args: any[]): {} }>(constructor: T) {
        return class extends constructor {
            pluginInfo = {
                id: pluginId
            };
        }
    }
}

export function Sender(streamId: StreamId = defaultStreamId) {
    return function (target: any, propertyKey: string) {
        const senders = target.Senders || [];
        target.Senders = [...senders, {property: propertyKey, streamId}];
    }
}

export function Receiver(streamId: StreamId = defaultStreamId) {
    return function (target: any, propertyKey: string) {
        const receivers = target.Receivers || [];
        target.Receivers = [...receivers, {property: propertyKey, streamId}];
    }
}

export function citadelPluginFrom<T extends CitadelPlugin>(
    channel: CitadelChannel,
    dispatcher: MessageDispatcher,
    plugin: CitadelPlugin
): T {
    const id = plugin["pluginInfo"]?.id;
    if (!id) {
        throw Error('Can\'t found Plugin Info');
    }
    const senders = plugin["Senders"] || [];
    const receivers = plugin["Receivers"] || [];
    const proxy = {};
    senders.forEach(sender => {
        proxy[sender.property] = async (message: string) => {
            const messageStr = JSON.stringify(packMessage(message, "REQUEST", id, sender.streamId));
            return new Promise((resolve, reject) => {
                let handler: (message: string) => void;
                const task = new Promise((res, rej) => {
                    handler = (message) => {
                        res(message);
                    };
                    dispatcher.getEvent(id, sender.streamId).plusAssign(handler);
                });
                channel.sendMessageAsync(messageStr)
                    .then(() => {
                        const timeout = new Promise(((res, rej) => setTimeout(res, 5000, 'timeout')));
                        Promise.race([task, timeout])
                            .then((result) => {
                                dispatcher.getEvent(id, sender.streamId).minusAssign(handler);
                                if (result == 'timeout') {
                                    reject(new Error('Response waiting timeout'));
                                } else {
                                    resolve(result);
                                }
                            });
                    })
                    .catch((err) => {
                        dispatcher.getEvent(id, sender.streamId).minusAssign(handler);
                        reject(err);
                    });
            });
        };
    });
    receivers.forEach(receiver => {
        proxy[receiver.property] = () => dispatcher.getResponsableEvent(id, receiver.streamId);
    });
    return proxy as T;
}
