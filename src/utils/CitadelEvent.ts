export default class CitadelEvent<T> {

    private list: Array<(T) => void>;

    constructor() {
        this.list = [];
    }

    plusAssign(listener: (T) => void) {
        this.list = [...this.list, listener];
    }

    minusAssign(listener: (T) => void) {
        this.list = this.list.filter(l => l != listener);
    }

    notifyAll(element: T) {
        this.list.forEach(listener => listener(element));
    }
}