export default class ResponsableEvent<E, R> {

    private event: (message: E) => R;

    constructor() {
        this.event = undefined;
    }

    invoke(element: E): R | undefined {
        if (this.event == undefined) {
            return undefined;
        } else {
            return this.event(element);
        }
    }

    set(event: (message: E) => R) {
        this.event = event;
    }

    reset() {
        this.event = undefined;
    }
}