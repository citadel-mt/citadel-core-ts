import CitadelEvent from "./utils/CitadelEvent";
import {CitadelChannel} from "./Channel";
import ResponsableEvent from "./utils/ResponsableEvent";
import {packMessage} from "./CitadelMessage";

export type StreamId = string
export type PluginId = string

export default class MessageDispatcher {

    private readonly events: Map<PluginId, Map<StreamId, CitadelEvent<any>>>;
    private readonly responsableEvents: Map<PluginId, Map<StreamId, ResponsableEvent<any, any>>>;

    constructor(channel: CitadelChannel) {
        this.events = new Map<PluginId, Map<StreamId, CitadelEvent<any>>>();
        this.responsableEvents = new Map<PluginId, Map<StreamId, ResponsableEvent<any, any>>>();

        channel.onReceiveMessageEvent.plusAssign(message => {
            const rawData = JSON.parse(message);
            const {targetId, type, payload} = rawData;
            if (targetId != undefined && type != undefined && payload != undefined) {
                const {streamId, value} = payload;
                if (streamId != undefined) {
                    switch (type) {
                        case "REQUEST": {
                            const responsableEvent = this.getResponsableEvent(targetId, streamId);
                            if (responsableEvent != undefined) {
                                const response = responsableEvent.invoke(value);
                                if (response != undefined) {
                                    channel.sendMessageAsync(JSON.stringify(packMessage(
                                        response,
                                        "RESPONSE",
                                        targetId,
                                        streamId
                                    )));
                                }
                            }
                        }
                            break;
                        case "RESPONSE": {
                            const event = this.getEvent(targetId, streamId);
                            if (event != undefined) {
                                event.notifyAll(value);
                            }
                        }
                            break;
                    }
                }
            }
        });
    }

    getEvent(pluginId: PluginId, streamId: StreamId): CitadelEvent<any> {
        if (this.events[pluginId] == undefined) {
            this.events[pluginId] = new Map<PluginId, CitadelEvent<any>>();
        }
        if (this.events[pluginId][streamId] == undefined) {
            this.events[pluginId][streamId] = new CitadelEvent();
        }
        return this.events[pluginId][streamId];
    }

    getResponsableEvent(pluginId: PluginId, streamId: StreamId): ResponsableEvent<any, any> {
        if (this.responsableEvents[pluginId] == undefined) {
            this.responsableEvents[pluginId] = new Map<PluginId, ResponsableEvent<any, any>>();
        }
        if (this.responsableEvents[pluginId][streamId] == undefined) {
            this.responsableEvents[pluginId][streamId] = new ResponsableEvent();
        }
        return this.responsableEvents[pluginId][streamId];
    }
}
