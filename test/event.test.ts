import CitadelEvent from "../src/utils/CitadelEvent";

describe('CitadelEvent', () => {
    it('plus/minus assign', () => {
        const event = new CitadelEvent<number>();
        let result = 0;
        const handler = (val: number) => {
            result += val;
        };
        event.plusAssign(handler);
        event.notifyAll(5);
        event.minusAssign(handler);
        event.notifyAll(5);
        expect(result).toEqual(5);
    });
});