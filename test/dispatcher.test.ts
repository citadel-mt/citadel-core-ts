import {CitadelChannel} from "../src/Channel";
import CitadelEvent from "../src/utils/CitadelEvent";
import {CitadelPlugin, citadelPluginFrom, PluginInfo, Receiver, Sender} from "../src/CitadelPlugin";
import {EmptyRequest, SimpleResponse} from "../src/CitadelMessage";
import ResponsableEvent from "../src/utils/ResponsableEvent";
import MessageDispatcher from "../src/MessageDispatcher";

describe('Dispatcher Test', () => {
    let sentMessage: string;

    class ChannelMock implements CitadelChannel {
        readonly onReceiveMessageEvent: CitadelEvent<string>;

        constructor() {
            this.onReceiveMessageEvent = new CitadelEvent<string>();
        }

        async sendMessageAsync(message: string): Promise<void> {
            return new Promise((resolve, reject) => {
                console.log(message);
                sentMessage = message;
                setTimeout(() => {
                    resolve();
                    this.onReceiveMessageEvent.notifyAll(`{"type":"RESPONSE","targetId":"Test","payload":{"streamId":"TEST_OUTCOMING","value":{"name":"Test response"}}}`);
                }, 1000);
            });
        }
    }

    interface TestMessage {
        readonly name: string
    }

    @PluginInfo("Test")
    class TestPlugin implements CitadelPlugin {

        @Sender("TEST_OUTCOMING")
        readonly sendTestMessageAsync: (message: EmptyRequest) => Promise<TestMessage>;

        @Receiver("TEST_INCOMING")
        readonly onReceiveTestEvent: () => ResponsableEvent<TestMessage, SimpleResponse>;
    }

    it('test send message', async () => {
        const channel = new ChannelMock();

        const plugin = citadelPluginFrom<TestPlugin>(
            channel,
            new MessageDispatcher(channel),
            new TestPlugin()
        );

        const result = await plugin.sendTestMessageAsync(new EmptyRequest());
        expect(result).toEqual({name: 'Test response'});
        expect(sentMessage).toEqual(`{"targetId":"Test","type":"REQUEST","payload":{"streamId":"TEST_OUTCOMING","value":{}}}`);
    }, 10000);

    it('test receive message', () => {
        const channel = new ChannelMock();

        const plugin = citadelPluginFrom<TestPlugin>(
            channel,
            new MessageDispatcher(channel),
            new TestPlugin()
        );

        plugin.onReceiveTestEvent().set((message: TestMessage) => {
            expect(message).toEqual({name: 'Test call'});
            return new SimpleResponse("OK");
        });

        channel.onReceiveMessageEvent.notifyAll(`{"targetId":"Test","type":"REQUEST","payload":{"type":"com.appspot.magtech.citadelcore.TestMessage","streamId":"TEST_INCOMING","value":{"name":"Test call"}}}`);
        setTimeout(() => {
            expect(sentMessage).toEqual(`{"targetId":"Test","type":"RESPONSE","payload":{"streamId":"TEST_INCOMING","value":{"status":"OK"}}}`);
        }, 2000);
    });
});